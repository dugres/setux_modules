from re import compile

from setux.core.module import Module
from setux.logger import debug


class Distro(Module):
    '''Update Config File (sed)
    '''

    def deploy(self, target, *, path, line=None, select=None, remove=None, sudo=None, report='quiet'):
        try:
            cont = self.target.read(path, sudo=sudo, report=report)
        except Exception: # file not found
            cont = ''
        cont = cont.strip()
        msg = [f'{path} <>']
        modified = False

        if select:
            sre = compile(select).search
            if not line:
                line = select
            updated, found = [], False
            for cur in cont.split('\n'):
                if sre(cur):
                    found = True
                    if cur == line:
                        updated.append(cur)
                    else:
                        msg.append(f' <  {cur}')
                        msg.append(f'  > {line}')
                        updated.append(line)
                        modified = True
                else:
                    updated.append(cur)
            if not found:
                updated.append(line)
                modified = True

        elif remove:
            sre = compile(remove).search
            updated = []
            for cur in cont.split('\n'):
                if sre(cur):
                    msg.append(f' <  {cur}')
                    modified = True
                else:
                    updated.append(cur)

        else:
            updated = cont.split('\n')
            msg.append(f'  > {line}')
            updated.append(line)
            modified = True

        if modified:
            target.write(path, '\n'.join(updated)+'\n', sudo=sudo, report=report)

        if report != 'quiet':
            debug('\n'.join(msg))

        return modified
