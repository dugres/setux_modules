# Prepare
`setux.modules.prepare`

[Setux] Prepare Module

Minimum System Requieremnts

[PyPI] - [Repo] - [Doc]


[Module] implementation

`setux.core.module.Module`



[PyPI]: https://pypi.org/project/setux_modules
[Repo]: https://bitbucket.org/dugres/setux_modules
[Doc]: https://setux-managers.readthedocs.io/en/latest/group
[Setux]: https://setux.readthedocs.io/en/latest

[Module]: https://setux-core.readthedocs.io/en/latest/module
