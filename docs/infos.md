# Infos
`setux.modules.infos`

[Setux] Infos Module

Show target infos

[PyPI] - [Repo] - [Doc]


[Module] implementation

`setux.core.module.Module`



[PyPI]: https://pypi.org/project/setux_modules
[Repo]: https://bitbucket.org/dugres/setux_modules
[Doc]: https://setux-managers.readthedocs.io
[Setux]: https://setux.readthedocs.io

[Module]: https://setux-core.readthedocs.io/en/latest/module
