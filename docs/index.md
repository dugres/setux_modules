# Setux Modules

[Setux] Modules

[PyPI] - [Repo] - [Doc]


## Installation

    $ pip install setux_modules


[PyPI]: https://pypi.org/project/setux_modules
[Repo]: https://gitlab.com/dugres/setux_modules
[Doc]: https://setux-modules.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest
